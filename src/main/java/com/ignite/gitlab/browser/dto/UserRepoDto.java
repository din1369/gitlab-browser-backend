package com.ignite.gitlab.browser.dto;


import java.util.List;

public class UserRepoDto implements BaseDto {

    String name;
    String full_name;

    List<RepoOwnerDto> owner;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public List<RepoOwnerDto> getOwner() {
        return owner;
    }

    public void setOwner(List<RepoOwnerDto> owner) {
        this.owner = owner;
    }
}
