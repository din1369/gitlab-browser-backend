package com.ignite.gitlab.browser.controller;

import com.ignite.gitlab.browser.service.UserService;
import com.ignite.gitlab.browser.util.jwt.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/user")
public class UserController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET,value ="/getUserRepo", produces={"application/json"})
    @ResponseBody
    public ResponseEntity getUserRepo(HttpServletRequest request) throws IOException, URISyntaxException {

        String token = request.getHeader(tokenHeader);
        String userName = jwtTokenUtil.getUserNameFromToken(token);
        String gitlabToken = jwtTokenUtil.getOauthTokenFromToken(token);
        return ResponseEntity.ok(userService.getUserRepoDetails(userName,gitlabToken));
    }

    @RequestMapping(method = RequestMethod.GET,value ="/searchRepo/{userName}", produces={"application/json"})
    @ResponseBody
    public ResponseEntity getSearchedRepo(@PathVariable("userName") String userName) throws IOException, URISyntaxException {

        return ResponseEntity.ok(userService.getSearchedRepoDetails(userName));
    }
}
