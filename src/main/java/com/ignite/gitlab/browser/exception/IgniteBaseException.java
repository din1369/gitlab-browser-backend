package com.ignite.gitlab.browser.exception;

public class IgniteBaseException extends RuntimeException{

    private String errorCode;

    public IgniteBaseException() {
    }

    public IgniteBaseException(String message) {
        super(message);
    }

    public IgniteBaseException(Throwable cause) {
        super(cause);
    }

    public IgniteBaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public IgniteBaseException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }


    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
