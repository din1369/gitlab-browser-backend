package com.ignite.gitlab.browser.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.ignite.gitlab.browser.util.ResponseWrapper;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestGlobalExceptionHandler {

    @ExceptionHandler({IgniteBaseException.class})
    @ResponseBody
    public ResponseWrapper handleVistaBaseException(IgniteBaseException ex) {
        return ResponseWrapper.failWithMessage(HttpStatus.INTERNAL_SERVER_ERROR.toString(),ex.getMessage());
    }

    @ExceptionHandler(SQLGrammarException.class)
    @ResponseBody
    public ResponseWrapper handleSQLException(SQLGrammarException ex) {
        return ResponseWrapper.failWithMessage(HttpStatus.INTERNAL_SERVER_ERROR.toString(),ex.getMessage());
    }


    @ResponseBody
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseWrapper invalidFormatException(InvalidFormatException ex) {
        return ResponseWrapper.failWithMessage(HttpStatus.BAD_REQUEST.toString(),ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseWrapper handleException(Exception ex) {
        return ResponseWrapper.failWithMessage(HttpStatus.INTERNAL_SERVER_ERROR.toString(),ex.getMessage());
    }

    @ExceptionHandler(RequiredRequestDataNotFoundException.class)
    @ResponseBody
    public ResponseWrapper requiredDataNotFoundException(RequiredRequestDataNotFoundException ex) {
        return ResponseWrapper.failWithMessage(HttpStatus.BAD_REQUEST.toString(),ex.getMessage());
    }

    @ExceptionHandler(ObjectNotFoundException.class)
    @ResponseBody
    public ResponseWrapper objectNotFoundException(ObjectNotFoundException ex) {
        return ResponseWrapper.failWithMessage(HttpStatus.BAD_REQUEST.toString(),ex.getMessage());
    }

    @ExceptionHandler(OAuth2AuthenticationProcessingException.class)
    @ResponseBody
    public ResponseWrapper OAuth2AuthenticationProcessingException(OAuth2AuthenticationProcessingException ex) {
        return ResponseWrapper.failWithMessage(HttpStatus.FORBIDDEN.toString(),ex.getMessage());
    }






}
