package com.ignite.gitlab.browser.exception;

public class ObjectNotFoundException extends IgniteBaseException {
    public ObjectNotFoundException(String message) {
        super(message);
    }
}
