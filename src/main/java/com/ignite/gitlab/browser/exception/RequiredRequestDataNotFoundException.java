package com.ignite.gitlab.browser.exception;

import java.util.List;

public class RequiredRequestDataNotFoundException extends IgniteBaseException {

    private Long resourceId;

    private String resourceCode;

    private List<Long> errorIds;

    public RequiredRequestDataNotFoundException(Long resourceId, String errorCode, String message) {
        super(message, errorCode);
        this.resourceId = resourceId;
    }

    public RequiredRequestDataNotFoundException(String resourceCode, String errorCode, String message) {
        super(errorCode, message);
        this.resourceCode = resourceCode;
    }

    public RequiredRequestDataNotFoundException(String errorCode, String message, List<Long> errorIds) {
        super(errorCode, message);
        this.resourceCode = resourceCode;
        this.errorIds = errorIds;
    }

    public RequiredRequestDataNotFoundException(String errorCode, String message) {
        super(message, errorCode);
    }

    public RequiredRequestDataNotFoundException(String message) {
        super(message);
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public List<Long> getErrorIds() {
        return errorIds;
    }

    public void setErrorIds(List<Long> errorIds) {
        this.errorIds = errorIds;
    }

}
