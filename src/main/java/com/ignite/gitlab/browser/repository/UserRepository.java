package com.ignite.gitlab.browser.repository;

import com.ignite.gitlab.browser.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsername(String username);
    Optional<User> findByGithubId(String id);
}
