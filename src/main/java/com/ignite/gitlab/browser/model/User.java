package com.ignite.gitlab.browser.model;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User extends IgniteBaseModel {

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "github_id", nullable = false)
    private String githubId;

    @Transient
    private String gitlabToken;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGithubId() {
        return githubId;
    }

    public void setGithubId(String githubId) {
        this.githubId = githubId;
    }

    public String getGitlabToken() {
        return gitlabToken;
    }

    public void setGitlabToken(String gitlabToken) {
        this.gitlabToken = gitlabToken;
    }
}
