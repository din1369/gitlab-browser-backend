package com.ignite.gitlab.browser.model;


import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public class IgniteBaseModel implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
