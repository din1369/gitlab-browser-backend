package com.ignite.gitlab.browser.service.impl;

import com.ignite.gitlab.browser.dto.UserRepoDto;
import com.ignite.gitlab.browser.model.User;
import com.ignite.gitlab.browser.repository.UserRepository;
import com.ignite.gitlab.browser.service.UserService;
import com.ignite.gitlab.browser.util.HttpUtil;
import com.ignite.gitlab.browser.util.jwt.JwtUser;
import com.ignite.gitlab.browser.util.oauth2.GithubOAuth2UserInfo;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@Transactional
@Service("userService")
public class UserServiceImpl extends DefaultOAuth2UserService implements UserService {

    @Autowired
    UserRepository userRepository;

    @Value("${github.repo.url}")
    private String host;

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return new JwtUser(user);
    }

    @Override
    public UserRepoDto[] getUserRepoDetails(String userName, String gitlabToken) throws URISyntaxException, IOException {
        URI githubRepoUri = new URIBuilder(host)
                .setPath("users/"+userName+"/repos")
                .setParameter("access_token",gitlabToken).build();
        return HttpUtil.getUserRepos(githubRepoUri);
    }

    @Override
    public UserRepoDto[] getSearchedRepoDetails(String userName) throws URISyntaxException, IOException {
        URI githubRepoUri = new URIBuilder(host)
                .setPath("users/"+userName+"/repos").build();
        return HttpUtil.getUserRepos(githubRepoUri);
    }

    @Override
    public OAuth2User loadUser(OAuth2UserRequest oAuth2UserRequest) throws OAuth2AuthenticationException {
        OAuth2User oAuth2User = super.loadUser(oAuth2UserRequest);

       return processOAuth2User(oAuth2UserRequest, oAuth2User);

    }

    private OAuth2User processOAuth2User(OAuth2UserRequest oAuth2UserRequest, OAuth2User oAuth2User) {
        GithubOAuth2UserInfo githubOAuth2UserInfo = new GithubOAuth2UserInfo(oAuth2User.getAttributes());
        User user;
        Optional<User> userOptional = userRepository.findByGithubId(githubOAuth2UserInfo.getId());
        if(!userOptional.isPresent()){
            user= createUser(githubOAuth2UserInfo);
        }else{
            user = userOptional.get();
        }
        user.setGitlabToken(oAuth2UserRequest.getAccessToken().getTokenValue());
        return new JwtUser(user);
    }

    private User createUser(GithubOAuth2UserInfo  githubOAuth2UserInfo){
        User user = new User();
        user.setUsername(githubOAuth2UserInfo.getName());
        user.setGithubId(githubOAuth2UserInfo.getId());
        return userRepository.save(user);

    }
}
