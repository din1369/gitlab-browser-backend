package com.ignite.gitlab.browser.service;

import com.ignite.gitlab.browser.dto.UserRepoDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.io.IOException;
import java.net.URISyntaxException;

public interface UserService extends UserDetailsService  {

    UserDetails loadUserByUsername(String username);

    UserRepoDto[] getUserRepoDetails(String userName, String gitlabToken) throws URISyntaxException, IOException;

    UserRepoDto[] getSearchedRepoDetails(String userName) throws URISyntaxException, IOException;
}
