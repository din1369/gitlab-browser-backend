package com.ignite.gitlab.browser.util.oauth2;

import java.util.Map;

public class GithubOAuth2UserInfo {

    protected Map<String, Object> attributes;

    public GithubOAuth2UserInfo(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public String getId() {
        return ((Integer) attributes.get("id")).toString();
    }

    public String getName() {
        return (String) attributes.get("login");
    }
}
