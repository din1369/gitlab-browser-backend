package com.ignite.gitlab.browser.util.jwt;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ignite.gitlab.browser.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Collection;
import java.util.Map;

public class JwtUser implements OAuth2User,UserDetails {

    private final Long id;
    private final String username;
    private final String token;
    private final Collection<? extends GrantedAuthority> authorities;
    private Map<String, Object> attributes;

    public JwtUser(
            Long id,
            String username,
            String token,
            Collection<? extends GrantedAuthority> authorities
    ) {
        this.id = id;
        this.username = username;
        this.token = token;
        this.authorities = authorities;
    }

    public JwtUser(User user){
        this.id = user.getId();
        this.username = user.getUsername();;
        this.token = user.getGitlabToken();
        this.authorities = null;
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }


    @Override
    public String getUsername() { return username; }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    public String getToken() {
        return token;
    }
}
