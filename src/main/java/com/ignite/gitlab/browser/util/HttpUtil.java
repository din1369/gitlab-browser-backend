package com.ignite.gitlab.browser.util;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.ignite.gitlab.browser.dto.UserRepoDto;
import com.ignite.gitlab.browser.exception.RequiredRequestDataNotFoundException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

public class HttpUtil {

    public static UserRepoDto[] getUserRepos(URI url) throws IOException {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = httpclient.execute(httpGet);

        try {
            String bodyAsString = EntityUtils.toString(response.getEntity());
            ObjectMapper mapper = new ObjectMapper();
            Map[] map = mapper.readValue(bodyAsString, Map[].class);
//            if(map.toString("message").equals(new String("Not Found"))){
//                throw  new RequiredRequestDataNotFoundException("User Not Found");
//            }
            return JsonWrapperUtil.toObjectUsingJackson(bodyAsString, UserRepoDto[].class);
        } finally {
            httpclient.close();
        }
    }



}
