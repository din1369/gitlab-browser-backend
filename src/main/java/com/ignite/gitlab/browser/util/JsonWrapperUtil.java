package com.ignite.gitlab.browser.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ignite.gitlab.browser.exception.IgniteBaseException;

import java.io.IOException;

public class JsonWrapperUtil {

    public interface Serializers {
        ObjectMapper JACKSON = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    public static <T extends Object> T toObjectUsingJackson(String json, Class<T> type) {

        try {
            return Serializers.JACKSON.readValue(json, type);
        } catch (IOException deserializationEx) {
            throw new IgniteBaseException("Error occurred while deserialize the json string",
                    deserializationEx);
        }
    }

}
