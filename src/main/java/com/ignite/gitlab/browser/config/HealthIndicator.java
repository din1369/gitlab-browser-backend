package com.ignite.gitlab.browser.config;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

@Component

public class HealthIndicator extends AbstractHealthIndicator {

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {

        builder.up()
                .withDetail("app", "App IS Alive and Kicking")
                .withDetail("error", "Nothing! I'm good.");
    }
}
